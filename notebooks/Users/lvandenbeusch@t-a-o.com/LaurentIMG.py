# Databricks notebook source
# DBTITLE 1,Bibliothèques Numpy lecture image
# import the necessary packages
import numpy as np
from urllib.request import urlopen
import cv2

# Definition des chemins qu'on va utiliser pour les parquet
import os
chemin_interim = '/default/Ludovic/INTERIM'

# COMMAND ----------

# DBTITLE 1,Connexion Snowflake
# Use secret manager to get the login name and password for the Snowflake user
user = dbutils.secrets.get(scope="snowflake", key="username")
password = dbutils.secrets.get(scope="snowflake", key="password")

# snowflake connection options
options = dict(sfUrl="tao.west-europe.azure.snowflakecomputing.com",
               sfUser=user,
               sfPassword=password,
               sfDatabase="TAODATA_PRD",
               sfSchema="DATASCIENCE_SB",
               sfWarehouse="WH_DATASCIENCE")

# COMMAND ----------

# DBTITLE 1,UDF de lecture et redimensionnement de l'image
#
# Lecture du fichier jpeg
# UDF pour sortir un tableau de composantes RGB
# redimensionné en size
#
from pyspark.sql.functions import udf
from pyspark.sql.types import ByteType,IntegerType,LongType,ArrayType

def url_to_image(url, size):
    # download the image, convert it to a NumPy array
    resp = urlopen(url)
    image = np.asarray(bytearray(resp.read()), dtype="uint8")
    image = cv2.imdecode(image, cv2.IMREAD_COLOR)
    
    # resize l'image sur le côté le plus long en size
    # size = 600
    if int(image.shape[1]) > int(image.shape[0]) :
      scale = size / int(image.shape[1])
    else :
      scale = size / int(image.shape[0])
      
    width = int(image.shape[1] * scale)
    height = int(image.shape[0] * scale)
    dim = (width, height)
    image = cv2.resize(image, dim, interpolation = cv2.INTER_AREA)
    
    # Convertit BGR en RGB
    image = cv2.cvtColor(image, cv2.COLOR_BGR2RGB).tolist()
    return image
  
def url_to_bytes(url):
    # download the image, convert it to a NumPy array
    resp = urlopen(url)
    image = np.asarray(bytearray(resp.read()), dtype="uint8").tolist()
    return image

url_to_image_udf = udf(lambda x,size: url_to_image(x,size))
url_to_bytes_udf = udf(lambda x: url_to_bytes(x))


# COMMAND ----------

# DBTITLE 1,Dataframe des URL images
#
# Lecture des URL présignées Snowflake
# Vue source : DATASCIENCE_SB.ITEMS_PICTURES
# Test sur la réf 25955, et la vue A
#
from pyspark.sql.functions import *

itemPicturesQuery = \
'''
select 
    A.ITEM_PRODUCT_COLOR,
    A.ITEM_PICTURE_VIEW,
    A.ITEM_PICTURE_URL
from DATASCIENCE_SB.ITEMS_PICTURES A
where NOT EXISTS (select 1 from DATASCIENCE_SB.ITEMS_PICTURES_RGB B
    where A.ITEM_PRODUCT_COLOR = B.ITEM_PRODUCT_COLOR
    and A.ITEM_PICTURE_VIEW = B.ITEM_PICTURE_VIEW)
    and A.ITEM_PICTURE_VIEW = 'B'
'''

dfItemPictures = spark.read \
  .format("snowflake") \
  .options(**options) \
  .option("query", itemPicturesQuery) \
  .load()

dfItemPictures.write.mode('overwrite').parquet(os.path.join(chemin_interim, 'dfItemPictures_snowflake.pqt'))
# display(dfItemPictures)


# COMMAND ----------

# DBTITLE 1,Conversion des URL en liste de tableaux de 3 octets (RGB)
#
# Lecture du dataframe
# et sauvegarde de la liste d'octets dans Snowflake
# Table cible : DATASCIENCE_SB.ITEMS_PICTURES_BINARY
#

dfItemPictures_snowflake = spark.read.parquet(os.path.join(chemin_interim, 'dfItemPictures_snowflake.pqt')).repartition(1000)
# dfItemPictures_snowflake = spark.read.parquet(os.path.join(chemin_interim, 'dfItemPictures_snowflake.pqt'))
# dfItemPictures_snowflake.show()
# dfItemPicturesBinary = dfItemPictures_snowflake.repartition(100) # A tester pour optimiser perf de l'UDF ?

dfItemPicturesBinary = dfItemPictures_snowflake.select([col('ITEM_PRODUCT_COLOR'),col('ITEM_PICTURE_VIEW'),col('ITEM_PICTURE_URL')]).withColumn('ITEM_600PX_RGB', url_to_image_udf(col('ITEM_PICTURE_URL'),lit(600)))
# dfItemPicturesBinary.show()
dfItemPicturesBinary.write.mode('overwrite').parquet(os.path.join(chemin_interim, 'dfItemPicturesBinary.pqt'))

parquetItemPicturesBinary = spark.read.parquet(os.path.join(chemin_interim, 'dfItemPicturesBinary.pqt')).drop(col('ITEM_PICTURE_URL'))
parquetItemPicturesBinary.printSchema() # Le ITEM_x_RGB est en String, il sera ensuite convertit en tableau par l'api json.loads
parquetItemPicturesBinary.show()

parquetItemPicturesBinary.write \
  .format("snowflake") \
  .options(**options) \
  .option("dbtable", "ITEMS_PICTURES_RGB") \
  .mode("append") \
  .save()


# COMMAND ----------

# DBTITLE 1,Boucle affichage des images depuis Snowflake
#
# Lecture des RGB stockées en varchar dans Snowflake
# Affichage des images correspondantes
#
from pyspark.sql.functions import *
import matplotlib.pyplot as plt
import numpy as np
import json
from pyspark.sql import functions as F


# Lecture des RGB depuis Snowflake
itemPicturesRgbQuery = \
'''
select 
    A.ITEM_PRODUCT_COLOR,
    A.ITEM_PICTURE_VIEW,
    A.ITEM_600PX_RGB
from DATASCIENCE_SB.ITEMS_PICTURES_RGB A
where A.ITEM_PRODUCT_COLOR like '25955%'
'''

dfItemPicturesRgb = spark.read \
  .format("snowflake") \
  .options(**options) \
  .option("query", itemPicturesRgbQuery) \
  .load()

# dtype pour le RGB
rgb = np.dtype((np.uint8))

# Conversion du dataframe en Pandas
pdItemPicturesRgbQuery = dfItemPicturesRgb.toPandas()
images = pdItemPicturesRgbQuery["ITEM_600PX_RGB"]

for img in images:
        image = json.loads(img) # Conversion de la liste RGB (au format string) en tableau
        image = np.asarray(image, dtype=rgb)
        plt.imshow(image)
        plt.show()


# COMMAND ----------

# DBTITLE 1,Boucle affichage des images depuis Parquet
import matplotlib.pyplot as plt
import numpy as np
import json
from pyspark.sql import functions as F

# dtype pour le RGB
rgb = np.dtype((np.uint8))

# lecture du parquet
images = spark.read.parquet(os.path.join(chemin_interim, 'dfItemPicturesBinary.pqt'))
images.printSchema()

images25955 = images.filter(images.ITEM_PRODUCT_COLOR == '25955_02570')

pdImages25955 = images25955.toPandas()
images = pdImages25955["ITEM_600PX_RGB"]

for img in images:
        image = json.loads(img) # Conversion de la liste RGB (au format string) en tableau
        image = np.asarray(image, dtype=rgb)
        plt.imshow(image)
        plt.show()


# COMMAND ----------

# DBTITLE 1,Test affichage
import matplotlib.pyplot as plt
import numpy as np
import cv2
import json
from pyspark.sql import functions as F
# Definition des chemins qu'on va utiliser pour les parquet
import os
chemin_interim = '/default/Ludovic/INTERIM'

# dtype pour le RGB
rgb = np.dtype((np.uint8))

def display_image(image):
    # display image
    # print(image.RGB.tolist())
    plt.imshow(np.asarray(bytearray(image.RGB), dtype="uint8"))
    plt.show()

def display_images(images, size=(10, 10)):
    """
        Fonction qui affiche les images.
    """
    if not isinstance(images, list):
        images = [images]
    
    for img in images:
        img = json.loads(img)
        img = np.asarray(img, dtype=rgb)
        plt.figure(figsize=size)
        plt.imshow(img)
        plt.show()

images = spark.read.parquet(os.path.join(chemin_interim, 'dfItemPicturesBinary.pqt'))
images.printSchema()

images25955 = images.filter(images.ITEM_PRODUCT_COLOR == '25955_02403')

pdImages25955 = images25955.toPandas()
image = pdImages25955["ITEM_600PX_RGB"][0]
print(image)
print(type(image))

image2 = json.loads(image)
print(image2)
print(type(image2))
rgb = np.dtype((np.uint8))
image3 = np.asarray(image2, dtype=rgb)
print(image3)
print(type(image3))

plt.figure(figsize=(1,1))
plt.imshow(image3)
plt.show()

#for img in images25955:
        #image = np.asarray(bytearray(F.col('RGB')), dtype="uint8")
        #image = cv2.imdecode(image, cv2.IMREAD_COLOR)
#        plt.imshow(bytearray(img))
#        plt.show()
        
#display_images(images25955)



# COMMAND ----------

# DBTITLE 1,Test Liste
import numpy as np
import json

tab = '[[[244, 245, 240], [244, 245, 240]]]'
tab2 = json.loads(tab)
print(type(tab2))
print(tab2)

rgb = np.dtype((np.uint8))
image = np.asarray(tab2, dtype=rgb)
print(type(image))
print(image)


# COMMAND ----------

# DBTITLE 1,Test sur une URL
import matplotlib.pyplot as plt
import numpy as np
from urllib.request import urlopen
import cv2
from PIL import Image, ExifTags

fn = 'path/to/some/file/tester.jpg'

def test_exifread():
    with open(fn, 'rb') as f:
        exif = exifread.process_file(f)

    for k in sorted(exif.keys()):
        if k not in ['JPEGThumbnail', 'TIFFThumbnail', 'Filename', 'EXIF MakerNote']:
            print( '%s = %s' % (k, exif[k]) )
            
resp = urlopen('https://c4cck9sfcb1stg.blob.core.windows.net/stageszz6c381bc0-b1b7-4f0e-84c2-cdd77eb7b236/25955_02403-1.jpg?se=2021-06-10T08%3A08%3A03Z&sp=r&spr=https&sr=b&sv=2019-10-10&sig=M%2F5IsV3%2By9OTZhg%2BGVyZABNBYYtnWwpo9EShFf3uHOw%3D')

          
image = np.asarray(bytearray(resp.read()), dtype="uint8")
image = cv2.imdecode(image, cv2.IMREAD_COLOR)

# img = resp.read()
# info = img._getexif()
# for k, v in info.items():
#     nice = TAGS.get(k, k)
#     print( '%s (%s) = %s' % (nice, k, v) )
    
# resize l'image sur le côté le plus long en 500px
if int(image.shape[1]) > int(image.shape[0]) :
      scale = 800 / int(image.shape[1])
else :
      scale = 800 / int(image.shape[0])
      
width = int(image.shape[1] * scale)
height = int(image.shape[0] * scale)
dim = (width, height)
image = cv2.resize(image, dim, interpolation = cv2.INTER_AREA)
    
# Convertit BGR en RGB
image = cv2.cvtColor(image, cv2.COLOR_BGR2RGB).tolist()
print(type(image))
print(image)

#plt.figure(figsize=(1,1))
plt.imshow(image)
plt.show()